import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const Time = ({ time, format }) => {
  const timeString = moment.unix(time).fromNow();
  return (
    <span className="time">{timeString}</span>
  );
};
Time.propTypes = {
  time: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  format: PropTypes.string
};

export default Time;