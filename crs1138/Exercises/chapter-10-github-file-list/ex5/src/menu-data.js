export let menu = [
  {
    id: 0,
    label: 'new',
    url: 'https://news.ycombinator.com/newest'
  },
  {
    id: 1,
    label: 'comments',
    url: 'https://news.ycombinator.com/comments'
  },
  {
    id: 2,
    label: 'show',
    url: 'https://news.ycombinator.com/show'
  },
  {
    id: 3,
    label: 'ask',
    url: 'https://news.ycombinator.com/ask'
  },
  {
    id: 4,
    label: 'jobs',
    url: 'https://news.ycombinator.com/jobs'
  },
  {
    id: 5,
    label: 'submit',
    url: 'https://news.ycombinator.com/submit'
  },
];