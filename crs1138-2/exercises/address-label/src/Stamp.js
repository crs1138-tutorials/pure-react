import React from 'react';

const Stamp = () => (
  <div className='stamp'>
    <span className='stamp__text'>Stamp</span>
  </div>
);

export default Stamp;