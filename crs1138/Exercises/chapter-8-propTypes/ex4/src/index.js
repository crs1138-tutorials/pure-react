import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './index.css';

function Poster({ posterInfo }){
  const {image, title, text} = posterInfo;
  return (
    <div className="poster">
      <Image url={image}/>
      <Title heading={title}/>
      <Text text={text}/>
    </div>
  );
}
Poster.propTypes = {
  posterInfo: PropTypes.object.isRequired
}

const Image = ({ url }) => (<img className="posterImage" src={url} alt="demotivational poster" />)
Image.propTypes = {
  url: PropTypes.string.isRequired
}

function Title({ heading }) {
  const firstLetter = heading.slice(0,1);
  const lastLetter = heading.slice(heading.length-1, heading.length);
  const midTitle = heading.slice(1, heading.length-1);
  return (
    <h1 className="title">
      <span className="capitals">{firstLetter}</span><span className="midTitle">{midTitle}</span><span className="capitals">{lastLetter}</span>
    </h1>
  );
}
Title.propTypes = {
  heading: PropTypes.string.isRequired
}

const Text = ({ text }) => (<p className="posterTxt">{text}</p>);
Text.propTypes = {
  text: PropTypes.string.isRequired
}
const posterTest = {
  image: 'https://i.imgflip.com/14p2yv.jpg',
  title: 'Unique',
  text: 'Just because you are unique does not mean you are useful'
}

ReactDOM.render(
  <Poster posterInfo={posterTest}/>,
  document.querySelector('#root')
);