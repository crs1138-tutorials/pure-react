import React from 'react';
import PropTypes from 'prop-types';
import Avatar from './Avatar';
import Message from './Message';
import NameWithHandle from './NameWithHandle';
import ReplyButton from './ReplyButton';
import RetweetButton from './RetweetButton';
import LikeButton from './LikeButton';
import MoreOptionsButton from './MoreOptionsButton';
import Time from './time';

function Tweet({ tweet }) {
  return (
    <div className="tweet">
      <Avatar hash={tweet.gravatar} />
      <div className="content">
        <NameWithHandle author={tweet.author} />
        <Time time={tweet.timestamp} />
        <Message text={tweet.message} />
        <div className="buttons">
          <ReplyButton />
          <RetweetButton count={tweet.retweets} />
          <LikeButton count={tweet.likes} />
          <MoreOptionsButton />
        </div>
      </div>
      </div>
  );
}

Tweet.propTypes = {
  tweet: PropTypes.object.isRequired
}

export default Tweet;