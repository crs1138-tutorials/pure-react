import React from 'react';
import ReactDOM from 'react-dom';
import Avatar from './Avatar';
import Message from './Message';
import NameWithHandle from './NameWithHandle';
import Time from './Time';
import md5 from 'md5';
import PropTypes from 'prop-types';
import {
  ReplyButton,
  RetweetButton,
  LikeButton,
  MoreOptionsButton
} from './Buttons';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

function Tweet({ tweet }) {
  return (
    <div className='tweet'>
      <Avatar hash={ tweet.gravatar } />
      <div className='content'>
        <NameWithHandle author={ tweet.author } /><Time time={ tweet.timestamp } />
        <Message text={ tweet.message } />
        <div className='buttons'>
          <ReplyButton />
          <RetweetButton count={ tweet.retweets } />
          <LikeButton count={ tweet.likes } />
          <MoreOptionsButton />
        </div>
      </div>
    </div>
  );
}
Tweet.propTypes = {
  tweet: PropTypes.shape({
    message: PropTypes.string.isRequired,
    author: PropTypes.shape({
      handle: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired
    }).isRequired,
    likes: PropTypes.number,
    retweets: PropTypes.number,
    timestamp: PropTypes.string.isRequired
  })
};

const getGravatarHash = ( email ) => md5( email.trim().toLowerCase() );

const author = {
  handle: 'crs1138',
  name: 'Jan Pozivil',
  email: 'crs1138@me.com'
};

const testTweet = {
  message: 'Something about cats.',
  gravatar: getGravatarHash( author.email ),
  author,
  likes: 2,
  retweets: 5,
  timestamp: '2018-08-15 17:13:37'
};

ReactDOM.render(<Tweet tweet={ testTweet } />, document.querySelector('#root'));
registerServiceWorker();
