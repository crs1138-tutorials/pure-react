import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ErrorBox from './ErrorBox';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<ErrorBox>The world is ending</ErrorBox>, document.getElementById('root'));
registerServiceWorker();
