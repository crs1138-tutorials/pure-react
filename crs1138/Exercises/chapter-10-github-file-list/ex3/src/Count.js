import React from 'react';

function Count({ count }) {
  if ( !!count && count > 0 ) {
    return (
      <span className="count">
        {count}
      </span>
    );
  } else {
    return null;
  }
}

export default Count;