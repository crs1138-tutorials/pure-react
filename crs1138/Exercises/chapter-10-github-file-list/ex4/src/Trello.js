import React from 'react';
import PropTypes from 'prop-types';
import List from './List';

const Trello = ({ lists }) => {

  return (
    <div className="trello">

      { lists.map( (list, i) => {
        //let listW = Math.floor(wrapperW / lists.length) - 10;
        //_(wrapperW);
        return (
          <List list={list} key={list.id} listW={'33%'} />
        )
      }) }
    </div>
  );
};

Trello.propTypes = {
  lists: PropTypes.array.isRequired
};

export default Trello;