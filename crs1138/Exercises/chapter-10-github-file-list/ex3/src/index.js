import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Tweet from './Tweet';
import './index.css';

let TweetList = ({ tweets }) => {
  return (
    <div className="tweet-list">
      {tweets.map( tweet =>
        <div key={tweet.id} style={{marginBottom: 20}}>
          <Tweet tweet={tweet} />
        </div>
      )}
    </div>
  );
}
TweetList.propTypes = {
  tweets: PropTypes.array.isRequired
};

var tweets = [
  {
    id: 0,
    message: "Something about cats.",
    gravatar: "82566e9441eec17bcccfafa02f79f4aa",
    author: {
      handle: "crs1138",
      name: "Jan Pozivil"
    },
    likes: 2,
    retweets: 23,
    timestamp: "2016-07-30 21:24:37"
  },
  {
    id: 1,
    message: "Something about dogs.",
    gravatar: "82566e9441eec17bcccfafa02f79f4aa",
    author: {
      handle: "crs1138",
      name: "Jan Pozivil"
    },
    likes: 5,
    retweets: 13,
    timestamp: "2017-07-30 21:24:37"
  },
  {
    id: 2,
    message: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel quasi autem ipsa voluptatem? Enim reiciendis laboriosam, alias tempore mollitia explicabo.",
    gravatar: "82566e9441eec17bcccfafa02f79f4aa",
    author: {
      handle: "crs1138",
      name: "Jan Pozivil"
    },
    likes: 5,
    retweets: 13,
    timestamp: "2017-07-30 21:24:37"
  },
];

ReactDOM.render(
  <TweetList tweets={tweets} />,
  document.querySelector('#root')
);