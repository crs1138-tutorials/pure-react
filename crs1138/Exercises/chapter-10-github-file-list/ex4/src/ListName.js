import React from 'react';
import PropTypes from 'prop-types';

const ListName = ({ children }) => (
  <h3 className="list__name">{ children }</h3>
);

ListName.propTypes = {
  children: PropTypes.string
};

export default ListName;