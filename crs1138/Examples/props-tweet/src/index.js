import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import './index.css';

function Tweet({ tweet }) {
  return (
    <div className="tweet">
      <Avatar hash={tweet.gravatar} />
      <div className="content">
        <NameWithHandle author={tweet.author} />
        <Time time={tweet.timestamp} />
        <Message text={tweet.message} />
        <div className="buttons">
          <ReplyButton />
          <RetweetButton count={tweet.retweets} />
          <LikeButton count={tweet.likes} />
          <MoreOptionsButton />
        </div>
      </div>
      </div>
  );
}


var testTweet = {
  message: "Something about cats.",
  gravatar: "82566e9441eec17bcccfafa02f79f4aa",
  author: {
    handle: "carperson",
    name: "IAMA Cat Person"
  },
  likes: 2,
  retweets: 23,
  timestamp: "2016-07-30 21:24:37"
};

function Avatar({ hash }) {
  let url = `https://www.gravatar.com/avatar/${hash}`;
  return (
    <img
      src={url}
      className="avatar"
      alt="avatar" />
  );
}

function Message({ text }) {
  return (
    <div className="message">
      {text}
    </div>
  );
}

function NameWithHandle({ author }) {
  const {name, handle} = author;
  return (
    <span className="name-with-handle">
      <span className="name">{name}</span>
      <span className="handle">@{handle}</span>
    </span>
  );
}

const Time = ({ time }) => {
  const timeString = moment(time).fromNow();
  return (
    <span className="time">{timeString}</span>
  );
};

const ReplyButton = () => (<i className="fa fa-reply reply-button"></i>);

function Count({ count }) {
  if ( count > 0 ) {
    return (
      <span className="count">
        {count}
      </span>
    );
  } else {
    return null;
  }
}

const RetweetButton = ({ count }) => (
  <span className="retweet-button">
    <i className="fa fa-retweet" />
    <Count count={count} />
  </span>
);

const LikeButton = ({ count }) => {
  return (
    <span className="like-button">
      <i className="fa fa-heart" />
      <Count count={count} />
    </span>
  );
};

const MoreOptionsButton = () => (<i className="fa fa-ellipsis-h more-options-button"></i>);

ReactDOM.render(

  <Tweet tweet={testTweet} />,
  document.querySelector('#root')
);