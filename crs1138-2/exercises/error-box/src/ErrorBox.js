import React from 'react';
import PropTypes from 'prop-types';

const ErrorBox = ({ children }) => (
  <div className="errorbox">
    <span className='errorbox__icon'><i className="fas fa-exclamation-triangle"></i></span>
    { children }
  </div>
);

ErrorBox.propTypes = {
  children: PropTypes.string.isRequired
};
export default ErrorBox;