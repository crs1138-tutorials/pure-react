import React from 'react';

const ListMoreBtn = () => (
  <div className="list__more">
    <button className="btn"><i className="fa fa-ellipsis-h"></i></button>
  </div>
);

export default ListMoreBtn;