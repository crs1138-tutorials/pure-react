import React from 'react';
import PropTypes from 'prop-types';
import ListName from './ListName';
import ListMoreBtn from './ListMoreBtn';
import CardList from './CardList';
import AddCardBtn from './AddCardBtn';

const List = ({list, listW}) => {
  return (
    <div className="list" style={{width: listW}}>
      <ListName>{list.name}</ListName>
      <ListMoreBtn />
      <CardList cards={list.cards} />
      <AddCardBtn />
    </div>
  );
};
List.propTypes = {
  list: PropTypes.shape({
    name: PropTypes.string.isRequired,
    cards: PropTypes.array
  })
};

export default List;