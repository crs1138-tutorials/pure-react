import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Time from './time';
import './index.css';

const FileList = ({ files }) => {
  return (
    <table className="file-list">
      <tbody>
        {files.map(file => (
          <FileListItem key={file.id} file={file}/>
        ))}
      </tbody>
    </table>
  );
};
FileList.propTypes = {
  files: PropTypes.array
};

const FileListItem = ({ file }) => {
  return (
    <tr className="file-list__item file" >
      <td className="file__icon">
        <FileIcon file={file} />
      </td>
      <td className="file__name">
        <FileName file={file} />
      </td>
      <td className="commit__message">
        <CommitMessage commit={file.latestCommit} />
      </td>
      <td className="commit__time">
        <Time time={file.modified} />
      </td>
    </tr>
  );
};
FileListItem.propTypes = {
  file: PropTypes.object.isRequired
};

const FileIcon = ({ file }) => {
  const icon = (file.type === 'folder') ? 'fa-folder' : 'fa-file-text-o';

  return ( <i className={`fa ${icon}`}></i> );
};
FileIcon.propType = {
  file: PropTypes.object.isRequired
};

const FileName = ({ file }) => ( <span>{file.name}</span> );
FileName.propType = {
  file: PropTypes.object.isRequired
};

const CommitMessage = ({ commit }) => ( <span>{commit.message}</span> );
CommitMessage.propTypes = {
  commit: PropTypes.object.isRequired
};

const testFiles = [
  {
    id: 1,
    name: 'src',
    type: 'folder',
    modified: '2016-07-11 21:24:00',
    latestCommit: {
      message: 'Initial commit'
    }
  },
  {
    id: 2,
    name: 'tests',
    type: 'folder',
    modified: '2016-07-11 21:24:00',
    latestCommit: {
      message: 'Initial commit'
    }
  },
  {
    id: 3,
    name: 'README',
    type: 'file',
    modified: '2017-07-18 22:23:00',
    latestCommit: {
      message: 'Added a readme'
    }
  }
];

ReactDOM.render(
  <FileList files={testFiles}/>,
  document.getElementById('root')
);
