import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Comment from './Comment.js';
import registerServiceWorker from './registerServiceWorker';

const comment = {
  author: 'crs1138',
  message: 'Lorem ipsum dolor et serem na to chlapi.',
  likes: 4
};

ReactDOM.render(
  <div>
    <Comment author={comment.author} message={comment.message} likes={comment.likes} />
    <Comment author='somebody' message='a likable message' likes={1} />
    <Comment author='mr_unpopular' message='unlikable message' />
    <Comment author='mr_unpopular' message='another unlikable message' likes={0} />
    <Comment author='error_missing_message' />
    <Comment message='mystery author' />
  </div>, document.getElementById('root'));
registerServiceWorker();
