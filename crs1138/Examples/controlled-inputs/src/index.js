import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

class ControlledInput extends React.Component {
  state = { text: '' };

  handleChange = (event) => {
    this.setState({
      text: event.target.value
    });
  };

  render() {
    return (
      <input type="text"
        value={this.state.text}
        onChange={this.handleChange} />
      );
  }
}

class TrickInput extends React.Component {
  state = { text: 'try typing something' };

  handleChange = (eve) => {
    this.setState({
      text: 'haha nope'
    });
  }

  render() {
    return (
      <input type="text"
        value={this.state.text}
        onChange={this.handleChange} />
    );
  }
}

class NoNumbersInput extends React.Component {
  state = { text: '' };

  handleChange = (eve) => {
    let text = eve.target.value;
    text = text.replace(/[0-9]/g, '');
    this.setState({ text });
  }

  render() {
    return (
      <input type="text"
        value={this.state.text}
        onChange={this.handleChange} />
    );
  }
}

class CreditCardInput extends React.Component {
  state = { text: '' };

  handleChange = (eve) => {
    let text = eve.target.value;
    text = text.replace(/[^\d]/g, '')
               .replace(/(.{4})/g, '$1 ');
    this.setState({ text });
  }

  render() {
    return (
      <input type="text"
        value={this.state.text}
        onChange={this.handleChange}
        maxLength="19" />
    );
  }
}

ReactDOM.render(<CreditCardInput />, document.getElementById('root'));
registerServiceWorker();
