import React from 'react';
import ReactDOM from 'react-dom';
import Avatar from './Avatar';
import Message from './Message';
import NameWithHandle from './NameWithHandle';
import Time from './Time';
import {
  ReplyButton,
  RetweetButton,
  LikeButton,
  MoreOptionsButton
} from './Buttons';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

function Tweet({ tweet }) {
  return (
    <div className='tweet'>
      <Avatar hash={ tweet.gravatar } />
      <div className='content'>
        <NameWithHandle author={ tweet.author } /><Time time={ tweet.timestamp } />
        <Message text={ tweet.message } />
        <div className='buttons'>
          <ReplyButton />
          <RetweetButton count={ tweet.retweets } />
          <LikeButton count={ tweet.likes } />
          <MoreOptionsButton />
        </div>
      </div>
    </div>
  );
}

const testTweet = {
  message: 'Something about cats.',
  gravatar: 'xyz',
  author: {
    handle: 'catperson',
    name: 'IAMA Cat Person'
  },
  likes: 2,
  retweets: 5,
  timestamp: '2018-08-15 17:13:37'
};

ReactDOM.render(<Tweet tweet={ testTweet } />, document.querySelector('#root'));
registerServiceWorker();
