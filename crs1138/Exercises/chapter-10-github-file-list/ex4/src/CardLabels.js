import React from 'react';
import PropTypes from 'prop-types';

const CardLabels = ({ labels }) => {
  let JSXlabels = ( labels && labels.length > 0 ) ? labels.map( label => (<span className="card__label" key={label.id} style={{backgroundColor: label.color}}></span>)) : null;

  return (
    <div className="card__labels">{JSXlabels}</div>
  );
};
CardLabels.propTypes = {
  cardLabels: PropTypes.array
};

export default CardLabels;