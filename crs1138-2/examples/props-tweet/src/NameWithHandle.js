import React from 'react';

function NameWithHandle({ author }) {
  const { name, handle } = author;
  return (
    <span className='nameHandle'>
      <span className='handle'>@{handle}</span> <span className='name'>{name}</span>
    </span>
  );
}

export default NameWithHandle;