import React from 'react';

const NavItem = ({ id, url, children }) => (
  <div className='navbar__item'><a key={id} href={url} title={children}>{children}</a></div>
);

export default NavItem;