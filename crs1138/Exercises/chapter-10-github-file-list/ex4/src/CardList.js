import React from 'react';
import PropTypes from 'prop-types';
import Card from './Card';

const CardList = ({ cards }) => {

  return (
    <ul className="cardlist">
      {
        cards.map(card => (
          <li key={card.id}>
            <Card card={card} />
          </li>
        ))
      }
    </ul>
  )
};
CardList.propTypes = {
  cards: PropTypes.array
};
export default CardList;