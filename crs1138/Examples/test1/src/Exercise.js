import React from 'react';
import { ListGroupItem } from 'reactstrap';

export class Exercise extends React.Component {
  constructor(props) {
    super(props);
    this.state = { selectedLengthIndex: 0 }
  }

  render() {
    return (
      <ListGroupItem className="exercise">
        <div className="row">
          <div className="col-2 exercise__handle">
            <i className="fa fa-arrows"></i>
          </div>
          <div className="col-8 exercise__title text-left">
           { this.props.title }
          </div>
          <div className="col-2 exercise__trash">
            <i className="fa fa-trash"></i>
          </div>
        </div>
        <div className="row">
        </div>
      </ListGroupItem>
    );
  }
};