import React from 'react';
import CartCounter from './CartCounter';


function Nav ({activeTab, onTabChange, items}) {
  const itemsCount = items.reduce((sum, item)=> {
    return sum + item.count;
  },0);

  const itemsTotal = items.reduce((sum, item) => {
    return sum + (item.price * item.count);
  }, 0);


  return (
    <nav className="App--nav">
      <CartCounter itemsCount={itemsCount} itemsTotal={itemsTotal.toFixed(2)} />
      <ul>
        <li className={`App--nav__item ${activeTab === 0 && 'selected'}`}><a onClick={()=> onTabChange(0)}>Items</a></li>
        <li className={`App--nav__item ${activeTab === 1 && 'selected'}`}><a onClick={()=> onTabChange(1)}>Cart</a></li>
      </ul>
    </nav>
  );
}


export default Nav;