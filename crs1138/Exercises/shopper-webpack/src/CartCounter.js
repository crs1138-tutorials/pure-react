import React from 'react';
import PropTypes from 'prop-types';

function CartCounter({itemsCount, itemsTotal}) {
  let itemsStr = itemsCount > 1 ? `items` : `item`;

  if (itemsCount > 0)
  {
    return (
    <span className="App--nav--cartCounter">
      <i className="fa fa-shopping-cart"></i>&nbsp;{ itemsCount }&nbsp;{ itemsStr }, ${itemsTotal}
    </span>
    );
  } else {
    return (
      <span className="App--nav--cartCounter">
      </span>
    );
  }
}

CartCounter.propType = {
  cartLength: PropTypes.array.isRequired
};

export default CartCounter;