import React from 'react';

const ReplyButton = () => (<i className="fa fa-reply reply-button"></i>);

export default ReplyButton;