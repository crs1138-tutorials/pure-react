import React from 'react';
import ReactDOM from 'react-dom';

function MyThing() {
  return (
    <div className="book">
      <div className="title">The Title</div>
      <div className="author">The Author</div>
      <ul className="stats">
        <li className="rating">5 stars</li>
        <li className="isbn">12-345678-910</li>
      </ul>
    </div>
  );
}

function MyThingJS() {
  return (
    React.createElement('div', {className: 'book'},
      React.createElement('div', {className: 'title'}, 'The Title'),
      React.createElement('div', {className: 'author'}, 'The Author'),
      React.createElement('ul', {className: 'stats'},
        React.createElement('li', {className: 'rating'}, '5 stars'),
        React.createElement('li', {className: 'isbn'}, '12-345678910')
      )
    )
  );
}

function MySpaces() {
  return (
    <div>
      <div>
        Newline
        Test
      </div>
      <div>
        Empty

        Newlines

        Here
      </div>
      <div>
        &nbsp;Non-breaking
        &nbsp;Spaces&nbsp;
      </div>
      <div>
        Line1
        {' '}
        Line2
      </div>
    </div>
  );
}

function Greeting() {
  var username = "Emma";
  return <span> { username ? 'Hello, ' + username : 'Not logged in.' } </span>;
}

function MyThings() {
  return (
    <div>
      <MySpaces />
      <MyThing />
      <MyThingJS />
    </div>
  );
}

function ArrayOfFail() {
  return [ <div>One</div>, <ul>Two</ul> ];
}

function TwoExpressions() {
  let x = false;
  return (
    <span>{ x && 5 }</span>
  );
}

function SayHi() {
  return (
    <span>{alert('Hi')}</span>
  );
}

function QuotedString() {
  return (
    <div>
      <p>"This string is quoted" and this is not.</p>
      <p>"Only quoted string"</p>
      <p>'Single quoted string'</p>
      <p>Not quoted string</p>
      <p>{"Quoted string in braces"}</p>
      <p>{"\"Quoted string in braces\""}</p>
      <p>{'Single quoted string in braces'}</p>
    </div>
  );
}

ReactDOM.render(
  <QuotedString />,
  document.getElementById('root')
);