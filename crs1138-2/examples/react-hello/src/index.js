import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

function HelloWorld() {
  return (
    <div>
      <Hello /> <World />!
    </div>
  );
}

function Hello() {
  return <span>Hello</span>
}

function World() {
  return <span>World</span>;
}

function SubmitButton() {
  const buttonLabel = "Submit";
  return (
    <button>{buttonLabel}</button>
  );
}

function App() {
  return (
    <div>
      <HelloWorld /><br />
      <SubmitButton />
    </div>
  );
}
ReactDOM.render(<App />, document.querySelector('#root'));
registerServiceWorker();
