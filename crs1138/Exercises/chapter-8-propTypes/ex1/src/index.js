import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './index.css';

function AddressLabel({ person }) {
  return (
    <div className="addresslabel">
      <Fullname fullname={person.fullname}/>
      <Street street={person.street}/>
      <div>
        <City city={person.city}/>, <PostCode postcode={person.postcode}/>
      </div>
    </div>
  );
}
AddressLabel.propTypes = {
  person: PropTypes.object.isRequired
}

function Fullname({ fullname }) {
  return (
    <div className="fullname">
      <span className="firstname">{fullname.firstName}</span> <span className="lastname">{fullname.lastName}</span>
    </div>
  );
}
Fullname.propTypes = {
  fullname: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired
  }).isRequired
}

// function Street({ street }) {}
const Street = ({ street }) => (<div className="street">{street}</div>);
Street.propTypes = {
  street: PropTypes.string.isRequired
}

// function City({ city }) {}
const City = ({ city }) => (<span className="city">{city}</span>);
City.propTypes = {
  city: PropTypes.string.isRequired
}

// function PostCode({ postcode }) {}
const PostCode = ({ postcode }) => (<span className="postcode">{postcode}</span>);
PostCode.propTypes = {
  postcode: PropTypes.string.isRequired
}

const testPerson = {
  fullname: {
    firstName: 'Jan',
    lastName: 'Pozivil'
  },
  street: 'C./ Parradas 26',
  city: 'Velez de Benaudalla',
  postcode: '18670'
}

ReactDOM.render(
  <AddressLabel person={testPerson}/>,
  document.querySelector('#root')
);