import React from 'react';
import PropTypes from 'prop-types';
import CardEditBtn from './CardEditBtn';
import CardLabels from './CardLabels';
import CardName from './CardName';

let Card = ({ card }) => (
  <div className="card">
    <CardEditBtn />
    <CardLabels labels={card.labels} />
    <CardName>{card.name}</CardName>
  </div>
);
Card.propTypes = {
  card: PropTypes.object
};

export default Card;