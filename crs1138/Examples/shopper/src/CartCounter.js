import React from 'react';
import PropTypes from 'prop-types';

function CartCounter({ cartLength }) {
  let itemsStr = cartLength > 1 ? `items` : `item`;
  return (
    <div className="CartCounter">
      <i className="fa fa-shopping-cart"></i>&nbsp;{ cartLength }&nbsp;{ itemsStr }
    </div>
  );
}

CartCounter.propType = {
  cartLength: PropTypes.array.isRequired
};

export default CartCounter;