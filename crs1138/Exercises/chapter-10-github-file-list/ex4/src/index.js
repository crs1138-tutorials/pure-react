import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Trello from './Trello';
import registerServiceWorker from './registerServiceWorker';

let testLists = [
  {
    id: 0,
    name: 'To Do',
    cards: [
      {
        id: 1,
        name: 'Hacker News',
        labels: [
          {
            id: 0,
            color: '#61BD4F',
            labelTitle: 'ch10-github-file-list'
          }
        ]
      },
      {
        id: 2,
        name: 'Pinterest',
        labels: [
          {
            id: 0,
            color: '#61BD4F',
            labelTitle: 'ch10-github-file-list'
          },
          {
            id: 1,
            color: '#F2D600',
            labelTitle: 'not finished'
          }
        ]
      },
      {
        id: 3,
        name: 'InternetRadio genre cloud',
        labels: [
          {
            id: 0,
            color: '#61BD4F',
            labelTitle: 'ch10-github-file-list'
          }
        ]
      },
      {
        id: 4,
        name: 'test',
        labels: [
        ]
      }
    ]
  },
  {
    id: 1,
    name: 'Doing',
    cards: [
      {
        id: 0,
        name: 'Create Trello like lists',
        labels: [
          {
            id: 0,
            color: '#61BD4F',
            labelTitle: 'ch10-github-file-list'
          }
        ]
      }
    ]
  },
  {
    id: 2,
    name: 'Done',
    cards: [
    ]
  },
];

ReactDOM.render(<Trello lists={testLists} />, document.getElementById('root'));
registerServiceWorker();
