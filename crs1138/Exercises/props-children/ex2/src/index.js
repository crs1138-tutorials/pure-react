import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './index.css';

const _ = (myVar) => console.log(myVar);

function Nav({ children }) {
  React.Children.forEach(children, (child, i) => {
    _(child);
    if (child.type !== NavItem ) {
      throw new Error('Nav component accepts only NavItems as children');
    }
  })

  let items = React.Children.toArray(children);
  for(let i=items.length-1; i>=1; i--) {
    items.splice(i,0,<span key={i} className='separator'>|</span>);
  }
  return (
    <nav className="menu">
      {items}
    </nav>
  );
}
Nav.propTypes = {
  children: PropTypes.node.isRequired
};


function NavItem({ children, url }) {
  return (
    <a href={url} title={children} className="menu__item">{children}</a>
  );
}
NavItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]).isRequired
};


ReactDOM.render(
  <Nav>
    <NavItem url='/'>Home</NavItem>
    <NavItem url='/about'>About</NavItem>
    <NavItem url='/contact'>Contact</NavItem>
  </Nav>,
  document.getElementById('root'));
