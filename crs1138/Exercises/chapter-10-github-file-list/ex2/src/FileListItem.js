import React from 'react';
import PropTypes from 'prop-types';
import FileIcon from './FileIcon';
import FileName from './FileName';
import CommitMessage from './CommitMessage';
import Time from './time';

const FileListItem = ({ file }) => {
  return (
    <tr className="file-list__item file" >
      <td className="file__icon">
        <FileIcon file={file} />
      </td>
      <td className="file__name">
        <FileName file={file} />
      </td>
      <td className="commit__message">
        <CommitMessage commit={file.latestCommit} />
      </td>
      <td className="commit__time">
        <Time time={file.modified} />
      </td>
    </tr>
  );
};
FileListItem.propTypes = {
  file: PropTypes.object.isRequired
};

export default FileListItem;