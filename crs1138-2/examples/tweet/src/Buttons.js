import React from 'react';

export const ReplyButton = () => (
  <i className='fa fa-reply reply-button'></i>
);

export const RetweetButton = () => (
  <i className='fa fa-retweet retweet-button'></i>
);

export const LikeButton = () => (
  <i className='fa fa-heart like-button'></i>
);

export const MoreOptionsButton = () => (
  <i className='fa fa-ellipsis-h more-options-button'></i>
);
