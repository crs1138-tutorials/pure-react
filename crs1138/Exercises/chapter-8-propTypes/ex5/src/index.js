import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import './index.css';

const _ = ( myVar ) => console.log(myVar);

function Email({email}) {
  const {sender, subject, date, message } = email;
  return(
    <div className="email">
      <Select />
      <Pin />
      <Sender sender={ sender } />
      <Subject subject={ subject }/>
      <EmailDate date={ date } />
      <Message message={ message } />
    </div>
  );
}
Email.propTypes = {
  email: PropTypes.object.isRequired
};

const Select = () => (<div className="email__region email__select"><input type="checkbox" /></div>);

const Pin = () => (<div className="email__region email__pin"><i className="fa fa-thumb-tack" aria-label="Pin to top"/></div>);

function Sender({ sender }) {
  const {name, email} = sender;
  return (
    <div className="email__region email__sender">
      <span className="label">From:</span> { name } &lt;{email}&gt;
    </div>
  );
}
Sender.propTypes = {
  sender: PropTypes.shape({
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired
  })
};

const Subject = ({ subject }) => (<div className="email__region email__subject"><span className="label">Subject:</span> {subject}</div>);
Subject.propTypes = {
  subject: PropTypes.string.isRequired
};

const EmailDate = ({ date }) => {
  let dateFormatted = moment(date).fromNow();
  _(moment().startOf('century').fromNow());
  return (<div className="email__region email__date">{dateFormatted}</div>)
};
Date.propTypes = {
  date: PropTypes.instanceOf(Date)
};

const Message = ({ message }) => (<div className="email__region email__message">{message}</div>);
Message.propTypes = {
  message: PropTypes.string.isRequired
};

const emailTest = {
  sender: {
    name: 'Jan Pozivil',
    email: 'info@planxdesign.eu'
  },
  subject: 'Lorem ipsum dolor sit amet, consectetur adipisicing.',
  date: '2000-01-01 00:00:00',
  message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis fugit temporibus officia, velit voluptate dolorum itaque, earum quod obcaecati labore blanditiis quae quas provident ut laudantium sint cumque repellendus! Incidunt, repellendus.'
}

ReactDOM.render(
  <Email email={ emailTest }/>,
  document.querySelector('#root')
);