import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Nav from './Navbar';
import registerServiceWorker from './registerServiceWorker';

const testNavbar = [
  {
    id: 1,
    label: 'Home',
    url: '/',
  },
  {
    id: 2,
    label: 'About',
    url: '/about',
  },
  {
    id: 3,
    label: 'Contact',
    url: '/contact',
  },
  {
    id: 4,
    label: 'Blog',
    url: '/blog',
  },
];

ReactDOM.render(<Nav items={ testNavbar } />, document.getElementById('root'));
registerServiceWorker();
