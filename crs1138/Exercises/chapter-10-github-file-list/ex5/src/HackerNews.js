import React from 'react';
import StoryList from './StoryList';
import Header from './Header';
import { stories } from './static-data';


const HackerNews = () => (
  <div className="hackernews">
    <Header />
    <main>
      <StoryList stories={stories} />
    </main>
  </div>
);

export default HackerNews;