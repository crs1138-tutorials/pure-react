import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './index.css';

function Envelope({ toPerson, fromPerson }) {
  return(
    <div className="envelope">
      <Stamp/>
      <div className="addresses">
        <AddressLabel person={toPerson}/>
        <AddressLabel person={fromPerson}/>
      </div>
    </div>
  );
}
Envelope.propTypes = {
  toPerson: PropTypes.object.isRequired,
  fromPerson: PropTypes.object.isRequired
};

const Stamp = () => (<img className="stamp" src="http://placehold.it/75x90" alt="stamp75x90"/>);

function AddressLabel({ person }) {
  let { fullname, street, city, postcode } = person;
  return (
    <div className="addresslabel">
      <Fullname fullname={fullname}/>
      <Street street={street}/>
      <div>
        <City city={city}/>, <PostCode postcode={postcode}/>
      </div>
    </div>
  );
}
AddressLabel.propTypes = {
  person: PropTypes.object.isRequired
}

function Fullname({ fullname }) {
  return (
    <div className="fullname">
      <span className="firstname">{fullname.firstName}</span> <span className="lastname">{fullname.lastName}</span>
    </div>
  );
}
Fullname.propTypes = {
  fullname: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired
  }).isRequired
};

// function Street({ street }) {}
const Street = ({ street }) => (<div className="street">{street}</div>);
Street.propTypes = {
  street: PropTypes.string.isRequired
};

// function City({ city }) {}
const City = ({ city }) => (<span className="city">{city}</span>);
City.propTypes = {
  city: PropTypes.string.isRequired
};

// function PostCode({ postcode }) {}
const PostCode = ({ postcode }) => (<span className="postcode">{postcode}</span>);
PostCode.propTypes = {
  postcode: PropTypes.string.isRequired
};

const testToPerson = {
  fullname: {
    firstName: 'Jan',
    lastName: 'Pozivil'
  },
  street: 'C./ Parradas 26',
  city: 'Velez de Benaudalla',
  postcode: '18670'
};

const testFromPerson = {
  fullname: {
    firstName: 'Aja',
    lastName: 'Svarovska'
  },
  street: 'Snehurcina 713',
  city: 'Liberec 15',
  postcode: '46015'
};

ReactDOM.render(
  <Envelope toPerson={testToPerson} fromPerson={testFromPerson}/>,
  document.querySelector('#root')
);