import React from 'react';
import PropTypes from 'prop-types';
import Count from './Count';

const RetweetButton = ({ count }) => (
  <span className="retweet-button">
    <i className="fa fa-retweet" />
    <Count count={count} />
  </span>
);
RetweetButton.propTypes = {
  count: PropTypes.number
};

export default RetweetButton;