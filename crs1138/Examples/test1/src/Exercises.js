import React from 'react';
import { Card, CardHeader, CardTitle, CardBlock, CardText, ListGroup, ListGroupItem } from 'reactstrap';
import { Exercise } from './Exercise';

export class Exercises extends React.Component {
  render() {
    return (
      <div>
        <Card>
          <CardHeader>
            <CardTitle>{this.props.title}</CardTitle>
          </CardHeader>
          <CardBlock className="card-body">
            <CardText>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati nisi laudantium adipisci a, sint necessitatibus, nihil beatae est voluptatibus repellendus eum excepturi asperiores consequuntur, reprehenderit dolores quo ex non magnam qui accusantium.
            </CardText>
          </CardBlock>
          <ListGroup className="list-group-flush">
            <Exercise id={1} title="Push ups" lengthRange={[25,35,50,120]} />
            <Exercise id={2} title="Jumping Jacks" lengthRange={[30,40,120,230]} />
          </ListGroup>
        </Card>
      </div>
    );
  }
}

