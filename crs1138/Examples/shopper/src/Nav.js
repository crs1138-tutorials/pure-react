import React from 'react';



function Nav ({activeTab, onTabChange, items}) {
  const itemsCount = items.reduce((sum, item)=> {
    return sum + item.count;
  },0);

  const itemsTotal = items.reduce((sum, item) => {
    return sum + (item.price * item.count);
  }, 0);
  let itemsStr = itemsCount > 1 ? `items` : `item`;

  return (
    <nav className="App--nav">
      <span className="App--nav--cartCounter">
        <i className="fa fa-shopping-cart"></i>&nbsp;{ itemsCount }&nbsp;{ itemsStr }, ${itemsTotal}
      </span>
      <ul>
        <li className={`App--nav__item ${activeTab === 0 && 'selected'}`}><a onClick={()=> onTabChange(0)}>Items</a></li>
        <li className={`App--nav__item ${activeTab === 1 && 'selected'}`}><a onClick={()=> onTabChange(1)}>Cart</a></li>
      </ul>
    </nav>
  );
}


export default Nav;