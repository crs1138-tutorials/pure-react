import React from 'react';
import ReactDOM from 'react-dom';
import FileList from './FileList';
import './index.css';

const testFiles = [
  {
    id: 1,
    name: 'src',
    type: 'folder',
    modified: '2016-07-11 21:24:00',
    latestCommit: {
      message: 'Initial commit'
    }
  },
  {
    id: 2,
    name: 'tests',
    type: 'folder',
    modified: '2016-07-11 21:24:00',
    latestCommit: {
      message: 'Initial commit'
    }
  },
  {
    id: 3,
    name: 'README',
    type: 'file',
    modified: '2017-07-18 22:23:00',
    latestCommit: {
      message: 'Added a readme'
    }
  }
];

ReactDOM.render(
  <FileList files={testFiles}/>,
  document.getElementById('root')
);
