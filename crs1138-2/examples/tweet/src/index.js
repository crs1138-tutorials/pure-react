import React from 'react';
import ReactDOM from 'react-dom';
import Avatar from './Avatar';
import Message from './Message';
import NameWithHandle from './NameWithHandle';
import Time from './Time';
import {
  ReplyButton,
  RetweetButton,
  LikeButton,
  MoreOptionsButton
} from './Buttons';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

function Tweet() {
  return (
    <div className='tweet'>
      <Avatar />
      <div className='content'>
        <NameWithHandle /><Time />
        <Message />
        <div className='buttons'>
          <ReplyButton />
          <RetweetButton />
          <LikeButton />
          <MoreOptionsButton />
        </div>
      </div>
    </div>
  );
}

ReactDOM.render(<Tweet />, document.querySelector('#root'));
registerServiceWorker();
