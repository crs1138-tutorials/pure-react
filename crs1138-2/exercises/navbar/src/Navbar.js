import React from 'react';
import NavItem from './NavItem';

const Nav = ({ items }) => (
  <nav className="navbar">
    {
      items.map(item => {
        const { id, url, label } = item;
        return (<NavItem key={id} url={url}>{label}</NavItem>);
      })
    }
  </nav>
);

export default Nav;