import React from 'react';

const MoreOptionsButton = () => (<i className="fa fa-ellipsis-h more-options-button"></i>);

export default MoreOptionsButton;