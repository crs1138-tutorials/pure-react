import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

function Greeting({ username }) {
  // const username = 'Honza';
  // const username = false;

  return (
    <span>
      {!username ? 'Not logged in' : `Hello ${username}`}
    </span>
  );
}

function MyThing() {
  return (
    <div className='book'>
      <div className='title'>
        The title
      </div>
      <div className='author'>
        The Author
      </div>
      <ul className='stats'>
        <li className='rating'>
          5 stars
        </li>
        <li className='isbn'>
          12-345678-910
        </li>
      </ul>
      <div>
        Newline
        Test
      </div>
      <div>
        Empty

        Newlines

        Here
      </div>
      <div>
        &nbsp;Non-breaking
        &nbsp;Spaces&nbsp;
      </div>
      <div>
        Line 1
        {' '}
        Line2
      </div>
      <div className='user'>
        <Greeting username='Honza' />
      </div>
    </div>
  );
}

function MyThing2() {
  return React.createElement('div', {className: 'book'},
      React.createElement('div', {className: 'title'}, 'The Title'),
      React.createElement('div', {className: 'author'}, 'The Author'),
      React.createElement('ul', { className: 'stats'},
        React.createElement('li', {className: 'rating'}, '5 stars'),
        React.createElement('li', {className: 'isbn'}, '12-345678-910')
      )
    );
}

function App() {
  return (
    <div>
      <MyThing /><br/>
      <MyThing2 />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
