import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './index.css';

function CreditCard({ cardInfo }) {
  let { cardholder, expiryDate, cardNumber, bankName } = cardInfo;
  return (
    <div className="creditCard">
      <Bank name={bankName}/>
      <CardNumber number={cardNumber}/>
      <Expires date={expiryDate}/>
      <Cardholder name={cardholder}/>
    </div>
  );
}
CreditCard.propTypes = {
  card: PropTypes.object
};

const Bank = ({ name }) => (<div className="bank">{name}</div>);
Bank.propTypes = {
  name: PropTypes.string.isRequired
};

function CardNumber({ number }) {
  const firstFourDigits = function(cardNumber) {
    return cardNumber.toString().slice(0,4);
  };

  const insertSpaces = function(cardNumber) {
    let cardNumberStr = cardNumber.toString();
    let subCardString = '';
    for (let i=0, ii=cardNumberStr.length; i < ii; i=i+4 ) {
      subCardString += cardNumberStr.slice(i, i+4) + ' ';
    }
    return subCardString;
  };

  return (
    <div className="cardNumber">
      { insertSpaces(number) }
      <div className="cardType">{ firstFourDigits(number) }</div>
    </div>
  );
}
CardNumber.propTypes = {
  number: PropTypes.number.isRequired
};

function Expires({ date }) {
  let { month, year } = date;
  const yearSlice = (year) => {
    year = year.toString();
    year = year.slice(2,4);
    year = parseInt(year, 10);
    year = year < 10 ? `0${year}` : year;
    return year;
  };
  month = month < 10 ? `0${month}` : month;
  year = yearSlice(year);
  return (
    <div className="expires">
      <span className="label">Valid thru</span>
      <span className="month">{month}</span>/<span className="year">{year}</span>
    </div>
  );
}
Expires.propTypes = {
  date: PropTypes.shape({
    month: PropTypes.number.isRequired,
    year: PropTypes.number.isRequired
  })
};

const Cardholder = ({ name }) => (<div className="cardholder">{name}</div>);
Cardholder.propTypes = {
  name: PropTypes.string.isRequired
};

const cardInfoTest = {
  cardholder: 'Mr. Jan Pozivil',
  expiryDate: {
    month: 8,
    year:  2019
  },
  cardNumber: 1234567887654321,
  bankName: 'Big Bank, Inc.'
}

ReactDOM.render(
  <CreditCard cardInfo={cardInfoTest}/>,
  document.querySelector('#root')
);