import React from 'react';
import { Exercises } from './Exercises';

export default class Controls extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <h1 className="text-center">{this.props.title}</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-4 ml-auto">
            <Exercises title="Workout Exercises" />
          </div>
          <div className="col-4 mr-auto">
            <Exercises title="Exercise Bank" />
          </div>
        </div>
      </div>
    );
  }
}