import React from 'react';
import PropTypes from 'prop-types';

const Address = ( { name, address}, typeClass = 'recipient' ) => {
  return (
    <div className={`${typeClass} address`}>
      <div className='name'>{ name }</div>
      <div className='street'>{ address.street1 }</div>
      <div className='street'>{ address.street2 }</div>
      <div className='town'>{ address.town }</div>
      <div className='postCode'>{ address.postCode }</div>
      <div className='country'>{ address.country }</div>
    </div>
  );
};

Address.propTypes = {
  name: PropTypes.string.isRequired,
  address: PropTypes.shape({
    street1: PropTypes.string.isRequired,
    street2: PropTypes.string,
    town: PropTypes.string.isRequired,
    postCode: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
  }).isRequired,
};

export default Address;
