import React from 'react';
import ReactDOM from 'react-dom';

function HelloWorld() {
  return (
    <div>
      <Hello /> <World />!
      <SubmitButton />
    </div>
    // React.createElement('div', {}, 'Hello', 'World')
    // React.createElement(string|element, [propsObject], [children...])
  );
}

function Hello() {
  return (
    <span>Hello</span>
  );
}

function World() {
  return (
    <span>World</span>
  );
}

function SubmitButton() {
  var buttonLabel = "Submit";
  return (
    <button>{buttonLabel}</button>
  );
}

ReactDOM.render(
  <HelloWorld />,
  document.querySelector('#root')
);