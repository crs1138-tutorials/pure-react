import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import HackerNews from './HackerNews';
import registerServiceWorker from './registerServiceWorker';

// var _ = (myVar) => console.log(myVar);

ReactDOM.render(<HackerNews />, document.getElementById('root'));
registerServiceWorker();
