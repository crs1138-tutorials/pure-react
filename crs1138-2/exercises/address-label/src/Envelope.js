import React from 'react';
import Address from './Addresses';
import Stamp from './Stamp';
import PropTypes from 'prop-types';

const Sender = ({ name, address }) => Address({ name, address }, 'sender');
const Recipient = ({ name, address }) => Address({ name, address }, 'recipient');

const Envelope = ({ data }) => {
  const { sender, recipient } = data;
  return (
    <div className='envelope'>
      <Sender name={ sender.name } address={ sender.address } />
      <Recipient name={ recipient.name } address={ recipient.address } />
      <Stamp />
    </div>
  );
};
Envelope.propTypes = {
  data: PropTypes.shape({
    sender: PropTypes.shape({
      name: PropTypes.string.isRequired,
      address: PropTypes.object.isRequired
    }).isRequired,
    recipient: PropTypes.shape({
      name: PropTypes.string.isRequired,
      address: PropTypes.object.isRequired
    }).isRequired,
  }).isRequired
};

export default Envelope;