import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function ErrorBox({ children }) {
  return (
    <div className="alert alert-danger" role="alert">
      <i className="fa fa-warning"></i> {children}
    </div>
  );
}

ReactDOM.render(
  <ErrorBox>
    Something has gone wrong
  </ErrorBox>,
  document.querySelector('#root')
);