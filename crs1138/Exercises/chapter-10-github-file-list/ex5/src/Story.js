// grayarrow - https://news.ycombinator.com/grayarrow.gif
import React from 'react';
import Time from './Time';
import PropTypes from 'prop-types';

let Story = ({ story }) => {
  let storyByLink = `https://news.ycombinator.com/user?id=${story.by}`;
  let hideLink = `https://news.ycombinator.com/hide?id=${story.id}&goto=news`;

  return (
    <div className="story__grid">
      <div className="story__upvote"><img src="https://news.ycombinator.com/grayarrow.gif" alt="upvote" /></div>
      <h3 className="story__title"><a href={story.url} title={story.title}>{story.title}</a></h3>
      <div className="story__details">
        <span className="story__points">{story.score}</span>&nbsp;points by&nbsp;<a href={storyByLink} className="story__author">{story.by}</a> <Time time={story.time} /> | <a href={hideLink} title="Hide story">hide</a> | <span className="story__comments">{story.descendants} comments</span>
      </div>
    </div>
  );
};

Story.propTypes = {
  story: PropTypes.object.isRequired
};

export default Story;