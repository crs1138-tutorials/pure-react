import React from 'react';

function Count({ count }) {
  return (count > 0) ? (<span className='count'>{count}</span>) : null;
}

export default Count;