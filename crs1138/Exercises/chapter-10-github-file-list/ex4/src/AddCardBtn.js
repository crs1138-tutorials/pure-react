import React from 'react';

const AddCardBtn = () => (
  <div className="list__add-card">
    <button className="btn btn__list">Add card&hellip;</button>
  </div>
);

export default AddCardBtn;