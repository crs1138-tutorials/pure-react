import React from 'react';
import PropTypes from 'prop-types';
import Count from './Count';

const LikeButton = ({ count }) => {
  return (
    <span className="like-button">
      <i className="fa fa-heart" />
      <Count count={count} />
    </span>
  );
};
LikeButton.propTypes = {
  count: PropTypes.number
};

export default LikeButton;