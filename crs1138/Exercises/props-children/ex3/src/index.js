import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './index.css';

function Dialog({ children }) {
  let header, body, footer;
  header, body, footer = null;

  React.Children.forEach(children, (child) => {
    switch (child.type)
    {
      case Header:
        header = child;
        break;
      case Body:
        body = child;
        break;
      case Footer:
        footer = child;
        break;
      default:
        throw new Error('The children of \'Dialog\' element must be either of these elements \'Header\', \'Body\' or \'Footer\'');
    }
  });

  return (
    <div className="modal">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            {header}
          </div>
          <div className="modal-body">
            {body}
          </div>
          <div className="modal-footer">
            {footer}
          </div>
        </div>
      </div>
    </div>
  );
}
Dialog.propTypes = {
  children: PropTypes.node
};

function Header({ children }) {
  return (
    <h5 className="modal-title">{children}</h5>
  );
}
Header.propTypes = {
  children: PropTypes.string.isRequired
};

const Body = ({ children }) => (<div>{children}</div>);
Body.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]).isRequired
}

const Footer = ({ children }) => (<div>{children}</div>);
Footer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ])
}

function Test() {
  return (
    <Dialog>
      <Header>Modal title</Header>
      <Body><p>Modal body text goes here.</p></Body>
      <Footer><button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button></Footer>
    </Dialog>
  );
}

ReactDOM.render(
  <Test/>,
  document.querySelector('#root')
);