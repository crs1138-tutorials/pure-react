import React from 'react';

function NameWithHandle() {
  return (
    <span className='nameHandle'>
      <span className='handle'>@handle</span> <span className='name'>Name</span>
    </span>
  );
}

export default NameWithHandle;