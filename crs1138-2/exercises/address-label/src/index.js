import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Envelope from './Envelope';
import registerServiceWorker from './registerServiceWorker';

const testData = {
  sender: {
    name: 'Jan Pozivil',
    address: {
      street1: 'C./ Parradas 26',
      street2: null,
      town: 'Vélez de Benaudalla',
      postCode: '18670',
      country: 'España',
    },
  },
  recipient: {
    name: 'Mary Plunkett',
    address: {
      street1: '13 Deancroft Road',
      street2: 'Chalfont St. Peter',
      town: 'Gerrards Cross',
      postCode: 'SL9 0HF',
      country: 'United Kingdom',
    },
  },
};

ReactDOM.render(<Envelope data={ testData } />, document.getElementById('root'));
registerServiceWorker();
