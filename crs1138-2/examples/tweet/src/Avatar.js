import React from 'react';

export default function Avatar() {
  return (
    <img src='https://www.gravatar.com/avatar/nothing' alt='Avatar' className='avatar' />
  );
}