import React from 'react';
const Content = ({isSidebarVisible, onShowSidebar, children}) => (
  <div className="content">
    { children }
    {!isSidebarVisible &&
      <button onClick={onShowSidebar}>Show</button>
    }
  </div>
);

export default Content;