import React from 'react';
import Story from './Story';
import PropTypes from 'prop-types';

let StoryList = ({ stories }) => {

  return (
    <ol className="story-list">
      { stories.map((story, index) => {
          return (
            <li key={story.id}><Story story={story} /></li>
          );
        }
      )}
    </ol>
  );
}
StoryList.propTypes = {
  stories: PropTypes.array
};

export default StoryList;