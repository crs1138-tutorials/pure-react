export let stories = [
  {
    "by" : "doener",
    "descendants" : 79,
    "id" : 15289689,
    "kids" : [ 15290101, 15290060, 15289970, 15291100, 15291236, 15290680, 15289997, 15291058, 15290148 ],
    "score" : 244,
    "time" : 1505864939,
    "title" : "US solar plant costs fall another 30 per cent in one year",
    "type" : "story",
    "url" : "http://reneweconomy.com.au/us-solar-plant-costs-fall-another-30-per-cent-just-one-year-49419/"
  },
  {
    "by" : "jackpot51",
    "descendants" : 28,
    "id" : 15290607,
    "kids" : [ 15290965, 15290616, 15290945, 15290829, 15291275, 15291044, 15291170, 15290814, 15290747, 15290831 ],
    "score" : 107,
    "time" : 1505878555,
    "title" : "Show HN: Redox Rust OS – v0.3.3 release",
    "type" : "story",
    "url" : "https://github.com/redox-os/redox/releases/tag/0.3.3"
  },
  {
    "by" : "mr_tyzic",
    "descendants" : 41,
    "id" : 15288486,
    "kids" : [ 15289369, 15289148, 15288779, 15289106, 15288726, 15290380, 15289978, 15288794, 15288941, 15288611, 15289181, 15288764, 15289429, 15289103 ],
    "score" : 301,
    "time" : 1505852877,
    "title" : "Clocks for Software Engineers",
    "type" : "story",
    "url" : "http://zipcpu.com/blog/2017/09/18/clocks-for-sw-engineers.html"
  },
  {
    "by" : "runesoerensen",
    "descendants" : 52,
    "id" : 15290200,
    "kids" : [ 15290282, 15290447, 15290384, 15290271, 15290376, 15290313, 15290805, 15290453, 15290824 ],
    "score" : 189,
    "time" : 1505872901,
    "title" : "Swift 4.0 Released",
    "type" : "story",
    "url" : "https://swift.org/blog/swift-4-0-released/"
  },
  {
    "by" : "brodock",
    "descendants" : 24,
    "id" : 15289676,
    "kids" : [ 15291330, 15290752, 15291166, 15290894, 15290780, 15290401 ],
    "score" : 139,
    "time" : 1505864603,
    "title" : "GraphQL Patent Infringement Issues",
    "type" : "story",
    "url" : "https://github.com/facebook/graphql/issues/351"
  },
  {
    "by" : "mathgenius",
    "descendants" : 41,
    "id" : 15289654,
    "kids" : [ 15289983, 15289957, 15290385, 15290051, 15290431, 15291256, 15290451, 15289902, 15290341, 15290375, 15290379, 15290154, 15289980 ],
    "score" : 94,
    "time" : 1505864077,
    "title" : "Consciousness Goes Deeper Than We Think",
    "type" : "story",
    "url" : "https://blogs.scientificamerican.com/observations/consciousness-goes-deeper-than-you-think/"
  },
  {
    "by" : "adamnemecek",
    "descendants" : 127,
    "id" : 15290117,
    "kids" : [ 15290745, 15290701, 15291177, 15290585, 15290597, 15290588, 15290647, 15290638, 15290821 ],
    "score" : 142,
    "time" : 1505871762,
    "title" : "China Bans Bitcoin Executives from Leaving Country, Miners “Preparing for Worst”",
    "type" : "story",
    "url" : "http://www.trustnodes.com/2017/09/19/china-bans-bitcoin-executives-leaving-country-miners-preparing-worst?"
  },
  {
    "by" : "ALee",
    "descendants" : 79,
    "id" : 15287376,
    "kids" : [ 15287924, 15288713, 15287696, 15287773, 15288809, 15288560, 15288774, 15288440, 15289710, 15288443, 15288659, 15288960, 15288658, 15288537, 15287779, 15288673, 15289471 ],
    "score" : 184,
    "time" : 1505845406,
    "title" : "Facebook Faces a New World as Officials Rein in a Wild Web",
    "type" : "story",
    "url" : "https://www.nytimes.com/2017/09/17/technology/facebook-government-regulations.html"
  },
  {
    "by" : "agronaut",
    "descendants" : 3,
    "id" : 15290718,
    "kids" : [ 15291326, 15291019 ],
    "score" : 17,
    "time" : 1505879967,
    "title" : "First quantum computers need smart software",
    "type" : "story",
    "url" : "https://www.nature.com/news/first-quantum-computers-need-smart-software-1.22590"
  },
  {
    "by" : "doener",
    "descendants" : 40,
    "id" : 15289564,
    "kids" : [ 15290902, 15290169, 15290448, 15290106, 15289955, 15290002, 15290839, 15290850, 15290744, 15290411, 15290256, 15291192, 15290340 ],
    "score" : 61,
    "time" : 1505862973,
    "title" : "Automotive Grade Linux Platform Debuts on the 2018 Toyota Camry",
    "type" : "story",
    "url" : "https://www.automotivelinux.org/announcements/2017/05/30/automotive-grade-linux-platform-debuts-on-the-2018-toyota-camry"
  }
];