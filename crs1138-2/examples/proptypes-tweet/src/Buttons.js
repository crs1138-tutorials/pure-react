import React from 'react';
import PropTypes from 'prop-types';
import Count from './Count';

export const ReplyButton = () => (
  <i className='fa fa-reply reply-button button'></i>
);

export const RetweetButton = ({ count }) => (
  <span className='retweet-button button'>
    <i className='fa fa-retweet'></i>
    <Count count={count} />
  </span>
);
RetweetButton.propTypes = {
  count: PropTypes.number
};

export const LikeButton = ({ count }) => (
  <span className='like-button button'>
    <i className='fa fa-heart'></i>
    <Count count={count} />
  </span>
);
LikeButton.propTypes = {
  count: PropTypes.number
};

export const MoreOptionsButton = () => (
  <i className='fa fa-ellipsis-h more-options-button button'></i>
);
