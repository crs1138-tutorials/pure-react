import React from 'react';

export default function Avatar({ hash }) {
  const url = `https://www.gravatar.com/avatar/${hash}`;
  return (
    <img
      src={ url }
      alt='Avatar'
      className='avatar' />
  );
}