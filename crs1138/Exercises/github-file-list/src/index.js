import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Time from './time';
import './index.css';

const FileList = ({ files }) => {
  return (
    <table className="file-list">
      <tbody>
        {files.map(file => (
          <FileListItem key={file.id} file={file}/>
        ))}
      </tbody>
    </table>
  );
};
FileList.propTypes = {
  files: PropTypes.array
};

const FileListItem = ({ file }) => {
  return (
    <tr className="file-list__item file" >
      { getFileName(file) }
      <CommitMessage commit={file.latestCommit} />
      <td className="commit__time"><Time time={file.modified} /></td>
    </tr>
  );
};
FileListItem.propTypes = {
  file: PropTypes.object.isRequired
};

const FileIcon = ({ file }) => {
  let icon = 'fa-file-text-o';
  if (file.type === 'folder') {
    icon = 'fa-folder';
  }

  return (
    <td className="file__icon"><i className={`fa ${icon}`}></i></td>
  );
};
FileIcon.propType = {
  file: PropTypes.object.isRequired
};

function getFileName(file) {
  return [
    <FileIcon file={file} key={0}/>,
    <td className="file__name" key={1}>{file.name}</td>
  ];
}

const CommitMessage = ({ commit }) => (<td className="commit__message">{commit.message}</td>);
CommitMessage.propTypes = {
  commit: PropTypes.object.isRequired
};

const testFiles = [
  {
    id: 1,
    name: 'src',
    type: 'folder',
    modified: '2016-07-11 21:24:00',
    latestCommit: {
      message: 'Initial commit'
    }
  },
  {
    id: 2,
    name: 'tests',
    type: 'folder',
    modified: '2016-07-11 21:24:00',
    latestCommit: {
      message: 'Initial commit'
    }
  },
  {
    id: 3,
    name: 'README',
    type: 'file',
    modified: '2017-07-18 22:23:00',
    latestCommit: {
      message: 'Added a readme'
    }
  }
];

ReactDOM.render(
  <FileList files={testFiles}/>,
  document.getElementById('root')
);
