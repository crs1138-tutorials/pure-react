import React from 'react';

const Navbar = ({ menu_items }) => {
  // for(let i=items.length-1; i>=1; i--) {
  //   items.splice(i,0,<span key={i} className='separator'>|</span>);
  // }

  return (
    <nav className="navbar">
      <ul className="menu">
        {
          menu_items.map((item, i) => {
            return (
              <li key={item.id} className="menu--item"><a href={item.label} title={item.url}>{item.label}</a>
              { ( i < menu_items.length-1 ) ? <span className="separator">|</span> : null }
              </li>
            )
          })
        }
      </ul>
    </nav>
  );
};

export default Navbar;