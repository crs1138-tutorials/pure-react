import React from 'react';

const CardEditBtn = () => (<button className="card__edit-btn btn"><i className="fa fa-pencil"></i></button>);

export default CardEditBtn;