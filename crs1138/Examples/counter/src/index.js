import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

// The first example
function handleAction(event) {
  console.log('Child did:', event);
}

function Parent() {
  return (
    <Child onAction={handleAction} />
  );
}

function Child({ onAction, onReset }) {
  return (
    <div>
      <button onClick={onAction}>Click me!</button>
      <button onClick={onReset}>Reset</button>
    </div>
  );
}

// The second example
class CountingParent extends React.Component {
  state = {
    actionCount: 0
  }

  handleAction = (action) => {
    console.log('Child says: ', action);
    this.setState((state, props) => {
      return {
        actionCount: this.state.actionCount + 1
      }
    })
  }

  resetCounter = (event) => {
    console.log(event);
    this.setState({actionCount: 0});
    // this.setState((state, props) =>{
    //   return {
    //     actionCount: 0
    //   }
    // });
  }

  render() {
    return (
      <div>
        <Child onAction={this.handleAction} onReset={this.resetCounter} />
        <p> Clicked {this.state.actionCount} times</p>
      </div>
    );
  }
}


const Page = () => (
  <div>
    <CountingParent />
    <CountingParent />
    <CountingParent />
  </div>
);

ReactDOM.render(<Page />, document.getElementById('root'));
registerServiceWorker();
