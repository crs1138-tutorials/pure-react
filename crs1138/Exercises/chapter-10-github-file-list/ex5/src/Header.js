import React from 'react';
import Navbar from './Navbar';
import {menu} from './menu-data';

const Header = () => (
  <header className="header">
    <img src="https://news.ycombinator.com/y18.gif" className="logo" alt="Hacker News logo" />
    <h1 className="site_title">Hacker News</h1>
    <Navbar menu_items={menu} />
    <a href="https://news.ycombinator.com/login?goto=news" title="login" className="login">login</a>
  </header>
);

export default Header;