import React from 'react';

const _ = (myVar) => console.log(myVar);

class App extends React.Component {
  state = {
    counter: 0
  };

  constructor(props) {
    super(props);
    _('Constructing...');
    _(`State already set to: `);
    _(this.state);
  }

  componentWillMount() {
    _(`About to mount...`);
  }

  componentDidMount() {
    _(`Mounted.`);
  }

  componentWillReceiveProps(nextProps) {
    _(`Current props:`);
    _(this.props);
    _(`Next props:`);
    _(nextProps);
  }

  shouldComponentUpdate(nextProps, nextState) {
    _(`Deciding to update…`);
    return (this.state.counter < 2) ? true : false;
  }

  componentWillUpdate(nextProps, nextState) {
    _(`About to update`);
    _(`Next Props:`);
    _(nextProps);
    _(`Next State:`);
    _(nextState);
  }

  componentDidUpdate() {
    _(`Updated.`);
  }

  componentWillUnmount() {
    _(`Goodbye cruel world`);
  }

  handleClick = () => {
    this.setState({
      counter: this.state.counter + 1
    });
  }

  resetCounter = () => {
    this.setState({
      counter: 0
    });
    this.componentWillUpdate(this.Props, this.State);
    this.render();
    this.componentDidUpdate();
  }

  render() {
    _(`Rendering…`);
    return (
      <div>
        <div>{ this.props.children }</div>
        <div>Counter: {this.state.counter}</div>
        <button onClick={this.handleClick}>Click to increment</button>
        <button onClick={this.resetCounter}>Reset</button>
      </div>
    );
  }
}

export default App;