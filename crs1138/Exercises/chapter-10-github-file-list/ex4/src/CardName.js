import React from 'react';
import PropTypes from 'prop-types';

const CardName = ({ children }) => (
  <div className="card__name">{children}</div>
);
CardName.propTypes = {
  children: PropTypes.string.isRequired
};

export default CardName;