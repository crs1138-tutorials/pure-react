import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';
import './CartPage.css';

const EmptyCart = () => {
  return (
    <div className="CartPage">
      <p className="CartPage__empty">
        Your cart is empty.
      </p>
      <p className="CartPage__empty">
        Why not add some expensive products to it?
      </p>
    </div>
  );
}
// class CartPage extends React.Component {
function CartPage({ items, onAddOne, onRemoveOne }) {

  let itemsTotal = items.reduce((sum, item) => {
    return sum + (item.count * item.price);
  }, 0);

    return (
      items.length === 0 ? <EmptyCart /> :
      <div className="CartPage">
        <ul className="CartPage--items">
          {items.map( item =>
            <li key={item.id} className="CartPage--item">
              <Item item={item}>
                <div className="CartItem__controls">
                  <button className="CartItem__removeOne" onClick={() => onRemoveOne(item)}>&ndash;</button>
                  <span className="CartItem__count">{item.count}</span>
                  <button className="CartItem__addOne" onClick={() => onAddOne(item)}>+</button>

                </div>
              </Item>
            </li>
          )}
        </ul>
        <h3 className="CartPage--total">
          Total: ${itemsTotal.toFixed(2)}
        </h3>
      </div>
    );
}
CartPage.propTypes = {
  items: PropTypes.array.isRequired,
  onAddOne: PropTypes.func.isRequired,
  onRemoveOne: PropTypes.func.isRequired
};

export default CartPage;